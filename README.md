# Criando ambiente virtual em python com PyQgis

Crie um ambiente virtual de usando o gerenciador de sua preferência.
No meu caso, o [PyQgis](https://qgis.org/pyqgis/master/) está instalado no [Python](www.python.org) 3.7, então gerei o ambiente usando esse python.

Obs: Estou usando fish shell.

## Primeiro: Criando o ENV

Abra o [Qgis](www.qgis.org), e no terminal [Python](www.python.org) dele verifique onde está instalado o modulo qgis..

```Python
>>> print(qgis.__file__)
```

No meu sistema (Linux DEBIAN) mostrou que o módulo está instalado na pasta `../../dist-packages/`

```Shell
python -m env testePyQgis # Criando um env limpo

source testePyQgis/bin/activate.fish # Ativando o env

pip list # Para ter certeza que o [Python](www.python.org) nao tem nada instalado.
```

Só para ter certeza que o [PyQgis](https://qgis.org/pyqgis/master/) não está sendo acessado pelo python. entre no shell do python e faça o import do qgis

```Python
>>> import qgis
# resposta
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
ModuleNotFoundError: No module named 'qgis'
>>>
```

Saia do terminal do [Python](www.python.org) (`exit()` ou ctrl+d).

## Segundo: Fazendo o python encontrar o PyQgis

Agora que já sabemos onde o [Qgis](www.qgis.org) está, podemos ir adicionando links simbólicos das dependencias que o qgis precisa para fazer o [PyQgis](https://qgis.org/pyqgis/master/) funcionar à pasta `site-packages` do ambiente virtual.

Eu coloquei link para os seguntes caminhos:

`../../dist-packages/qgis`
`../../dist-packages/PyQt5`
`../../dist-packages/sip.pyi`
`../../dist-packages/sip.cpython-37m-x86_64-linux-gnu.so`
`../../dist-packages/osgeo`

Usando o comando:  

```Shell
ln -s endereço/completo/até/a/pasta endereco/pasta/site/packages/do/ambiente/virtual
```

Entre no [Python](www.python.org) novamente e tente fazer o import do qgis

```Python
>>> import qgis
>>>
```

## Observações

Uma outra forma de testar os imports no python, sem precisar de abrir ele no terminal, é usar o comando:  

```Shell
python -c "import <pacote>"
```

Isso faz com que o terminal execute o comando que estiver entre as aspas direto no python, e em caso de erro, ele mostra o erro normalmente no terminal.  
Lembre-se de substituir `<pacote>` pelo pacote que quer testar o import.

Algumas dependencias que o [Qgis](www.qgis.org) precisa para executar podem ser instaladas no ambiente tambem.  
Como o `psycopg2` que é usando para acessar o `PostgreSQL/Postgis`, dentre outros.

Arquivo de requerimentos do [Qgis](www.qgis.org) (Oficial) [requirements.txt](https://github.com/qgis/QGIS/blob/master/requirements.txt)

Ao tentar executar o [Qgis](www.qgis.org) pelo terminal com o ambiente ativado, fará com que ele rode utilizando o python do ambiente ativado, e não o global.

### Iniciando o qgis sem plugins ativados

Alguns plugins precisam de dependências instaladas no [Python](www.python.org), como numpy, requests e outros. portanto, o [Python](www.python.org) do [Qgis](www.qgis.org) pode reclamar a falta de dependencias que esses plugins necessitam, o interessante seria iniciar uma instancia limpa do [Qgis](www.qgis.org) usando o comando `qgis --noplugins` no terminal.

Problemas encontrados ([Veja a ToDo List](https://gitlab.com/kylefelipe/ambiente-virtual-python-pyqgis/-/boards)) :

- [X] Após a exportação o pip não funciona (`ModuleNotFoundError: No module named 'pip._vendor.packaging'`)

Não consegui fazer o [PyQgis](https://qgis.org/pyqgis/master/) rodar em outras versões do [Python](www.python.org), como o [Python](www.python.org) 3.8 dessa forma.
